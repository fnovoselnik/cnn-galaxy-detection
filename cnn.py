# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 21:31:50 2018

"""

from skimage import io
from matplotlib import pyplot as plt
import matplotlib.cm as cm
import os
import cv2
import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import train_test_split

from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D, Lambda
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator


def rename_images(path, sufix):
    for root, dirs, files in os.walk(path):
        for filename in files:
            fname = filename.split('.')
            os.rename(os.path.join(path,filename), os.path.join(path,fname[0] + sufix + '.jpg'))
    


def prepare_data(source_path, destination_path, sufix):
        for root, dirs, files in os.walk(source_path):
            for filename in files:
                fname = filename.split('.')
                
                img_raw = io.imread(os.path.join(source_path,filename))
                w,h= img_raw.shape
                
                width = int(w/2)
                height = int(h/2)
                
                img_crop = img_raw[width-50:width+50, height-50:height+50]
                plt.imsave(destination_path + fname[0] + sufix, img_crop, cmap=cm.gray)
                

def load_data(path):
    X = []
    y = []
    for root, dirs, files in os.walk(path):
        for filename in files:
            if filename.endswith('.jpg'):
                label_tmp = filename.split('.')
                label_tmp[0] = label_tmp[0].split('_')
                label = label_tmp[0][1]
                print (label)

                img_fname = os.path.abspath(os.path.join(root, filename))
                
                img_file = cv2.imread(img_fname)
                img_arr = np.asarray(img_file)
                X.append(img_arr)
                y.append(label)
                
    X = np.asarray(X)
    y = np.asarray(y)
    
    
    encoder = LabelEncoder()
    encoder.fit(y)
    encoded_y = encoder.transform(y)
    
    y = np_utils.to_categorical(encoded_y)
    
    return X,y
    


def getModel(batch_size, nClasses, imgWidth, imgHeight, nChannels):
    model = Sequential()
    model.add(Lambda(lambda x: x/127.5 - 1., input_shape=(imgWidth, imgHeight, nChannels), output_shape=(imgWidth, imgHeight, nChannels)))
    
    model.add(Conv2D(32, (3, 3), input_shape=(imgWidth, imgHeight, nChannels)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(nClasses))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy',
                optimizer='rmsprop',
                metrics=['accuracy'])

    return model



def train_model(X, y, model):
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=10)
    
    datagen = ImageDataGenerator(
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True)
    
    train_generator = datagen.flow(
        X_train,
        y_train,
        batch_size=batch_size)

    validation_generator = datagen.flow(
        X_test,
        y_test,
        batch_size=batch_size)
    
    model_training = model.fit_generator(
        train_generator,
        steps_per_epoch=len(X_train),
        validation_data=validation_generator,
        validation_steps=len(X_test),
        epochs=epochs,
        # callbacks=[checkpoint]
        )
    model.save_weights('trained_models\\trained_weights_cnn.h5')
    
    
def predict(model, model_path, image_path):
    model.load_weights(model_path)
    
    image_path = 'C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\test_images\\no-galaxy\\'
    
    # img_fname_galaxy = 'C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\test_images\\galaxy\\UGC12818_galaxy.jpg'
    # img_fname_no_galaxy = 'C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\test_images\\no-galaxy\\0-40_no-galaxy.jpg'

    for fname in os.listdir(image_path):
        X = []
        
        img_file = cv2.imread(image_path + fname)
        img_arr = np.asarray(img_file)
        X.append(img_arr)
        
        X = np.asarray(X)
        y_pred = np.rint(model.predict(X))
        # y_pred_test = model.predict(X)
        
        if (y_pred[0][0] == 1):
            print ('galaxy: ', fname)
        if (y_pred[0][1] == 1):
            print ('no-galaxy: ', fname)
    
    
    
    
    
if __name__=="__main__":
    # Define model parameters

    batch_size = 32
    nClasses = 2
    epochs = 2
    imgWidth = 100
    imgHeight = 100
    nChannels = 3
    
    # rename_images(path='C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\dataset\\galaxy', sufix='_galaxy')
    # crop_image(img_fname)
    # prepare_data(source_path='C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\original_dataset\\galaxy\\',
    #              destination_path='C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\dataset\\galaxy\\', sufix='_galaxy.jpg')
    
    # X,y = load_data(path='C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\dataset\\')
    
    model = getModel(batch_size, nClasses, imgWidth, imgHeight, nChannels)
    # train_model(X, y, model)
    
    predict(model, model_path='trained_models\\trained_weights_cnn.h5',
            image_path = 'C:\\Users\\novos\\Dropbox\\ZECV\\VSA2018\\python\\CNN\\test_images\\no-galaxy\\')
    
    

    
    
    
    



